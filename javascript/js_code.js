$(document).ready(function () {
    var $navLinks = $("nav a");
    // Add smooth scrolling to all links
    $navLinks.on('click', function (event) {

        // Make sure this.hash has a value before overriding default behavior
        if (this.hash !== "") {
            // Prevent default anchor click behavior
            event.preventDefault();

            // Store hash
            var hash = this.hash;
            var this_offset = $(this).offset();
            var that_id = $(this).attr("href");
            var that_offset = $(that_id).offset();
            var offset_diff = Math.abs(that_offset.top - this_offset.top);

            var base_speed = 3500; // Time in ms per 1,000 pixels
            var speed = (offset_diff * base_speed) / 5920;

            // Using jQuery's animate() method to add smooth page scroll
            // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
            $('html, body').animate({
                scrollTop: $(hash).offset().top - 115
            }, speed, function () {

                // Add hash (#) to URL when done scrolling (default click behavior)
                window.location.hash = hash;
            });
        } // End if
    });
//Check to see if the window is top if not then display button
    $(window).scroll(function () {
        if ($(this).scrollTop() > 100) {
            $('.scrollToTop').fadeIn();
        } else {
            $('.scrollToTop').fadeOut();
        }
    });

    //Click event to scroll to top
    $('.scrollToTop').click(function () {
        $('html, body').animate({scrollTop: 0}, 800);
        return false;
    });
});